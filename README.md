# QrcodePoster
uni-app 生成canvas分享海报插件

QrcodePoster 生成方式简单，通过传相应的参数就可快速生成海报，并可以保存到本地相册，如有复杂需求可通过自定义组件或修改源码完成需求。

### 使用方式

在 ``script`` 中引用组件

```javascript
import QrcodePoster from '@/components/zhangyu-qrcode-poster.vue'
```

在 ``template`` 中创建 ``<qrcode-poster />``并传参

```html
<qrcode-poster ref="poster" :title="goods.title" 
		:subTitle="goods.sub_title" 
		:headerImg="goods.image[0]"
		:price="goods.price"></qrcode-poster>
```

在 ``script`` 中调用 ``sharePoster()`` 方法使其展示

```javascript
export default {
  methods: {
    //分享海报
    sharePoster(){
    	//获取带参数二维码
    	this.is_show_model = false
    	this.$refs.poster.showCanvas('https://oss.zhangyubk.com/cmqrcode.jpg')
    }
  }
}
```

### 属性说明

|参数|类型|必填|说明|
|---|---|---|---|
|title|String|是|海报标题|
|subTitle|String|是|海报副标题|
|headerImg|String|是|海报标题图片|
|price|Number|是|商品价格|
|abImg|String|否|底部广告图，可自行修改|

### 方法说明

|方法名|说明|
|---|---|
|[sharePoster](#sharePoster)|控制组件显示隐藏,需要传递带参数二维码路径|


### 完整示例

```html
<template>
	<view>
		<!--分享-->
		<view class="share" @click="is_show_model=true">有奖分享</view>
		<!--模态弹窗-->
		<view class="cu-modal bottom-modal" :class="[is_show_model?'show':'']" @click.stop="is_show_model=false">
		  <view class="cu-dialog bottom-md" style="border-top-left-radius: 20rpx;border-top-right-radius: 20rpx;" @click.stop="">
		    <view class="md-content">
		      <view class="md-content-item">
		        <image open-type="share" src="../../static/wechat.png"></image>
		        <view>分享给好友</view>
		        <button open-type="share" @click="is_show_model=false" class="sharebtn"></button>
		      </view>
		      <view class="md-content-item" @click="sharePoster">
		        <image src="../../static/haibao.png"></image>
		        <view>生成海报</view>
		      </view>
		    </view>
		    <view class="cancel" @click="is_show_model=false">取消</view>
		  </view>
		</view>
		<qrcode-poster ref="poster"></qrcode-poster>
	</view>
</template>

<script>
	import QrcodePoster from '../../components/zhangyu-qrcode-poster.vue'
	export default {
		components:{
			QrcodePoster
		},
		data() {
			return {
				is_show_model: false//是否显示分享模态窗
			}
		},
		methods: {
            //分享海报
			sharePoster(){
				//获取带参数二维码并传递
				this.is_show_model = false
				this.$refs.poster.showCanvas()
			}
		}
	}
</script>

<style>
.share{
	position: fixed;
	color: #FFFFFF;
	right: 0;
	bottom: 190rpx;
	background: linear-gradient(to bottom right, #FE726B, #FE956B);
	padding: 10rpx 10rpx 10rpx 20rpx;
	border-top-left-radius: 50px;
	border-bottom-left-radius: 50px;
	box-shadow: 0 0 20upx rgba(0,0,0,.09);
}
.cancel{
  width: 100vw;
  padding: 30rpx;
  text-align: center;
  background: #FFFFFF;
  color: red;
  font-weight: bold;
  font-size: 30rpx;
}
.md-content{ 
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 50rpx 0;
}
.md-content-item{
  margin: 0 70rpx;
  position: relative;
}
.md-content-item image{
  width: 100rpx;
  height: 100rpx;
}
.md-content-item view{
  margin-top: 15rpx;
  font-size: 28rpx;
}
.sharebtn{
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  opacity: 0;
}
</style>

```

**Tips**
- 大家请根据项目实际业务需求自行修改示例Demo的内容
